<html>
<head>
	<meta charset="UTF-8">
	<title> Refer | Greymatter Agency</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" type="text/css" href="css/style.css">
  	<link rel="stylesheet" type="text/css" href="css/bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="css/animate/animate.min.css">
  	<link rel="stylesheet" href="css/fonts/font.css">
  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.2.1/bootstrap-hover-dropdown.min.js"></script>
  	<script src="js/jquery-1.11.1.min.js"></script>
      <script src="js/responsiveslides.min.js"></script>
      <script type="text/javascript" src="css/bootstrap/js/hover.js"></script>
      <script src="css/bootstrap/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.0/jquery.flexslider.js"></script>
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
  	function hideURLbar(){ window.scrollTo(0,1); } 
  	</script>
</head>
<body>
	<div class="refer-and-earn-banner">
		<div class="portfolio-overlay">
			<div class="main-content">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<div class="first-div-content wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInLeft;">
								<small>
									Welcome to Greymatter Agency - Website development | Digital Marketing | Branding | Mobile apps
								</small>
							</div>
						</div>
						<div class="col-md-5">
							<div class="second-div-content  wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInRight;">
								<ul>
									<li class="number">08140396480</li>
									<a href=""><li>Blog</li></a>
									<a href=""><li>Training</li></a>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<nav class="navbar navbar-inverse wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInRight;">
						<div class="container-fluid">
						    <div class="navbar-header">
						      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						      </button>
						      <a class="navbar-brand" href="index.html"><img src="images/grey.png" style="object-fit: cover;height: 120px;"></a>
						    </div>
						    <div class="collapse navbar-collapse" id="myNavbar">
						      	<ul class="nav navbar-nav">
						      	</ul>
						      	<ul class="nav navbar-nav navbar-right">
						      		<li class="dropdown">
						              <a href="#" class="dropdown-toggle nav-a" data-toggle="dropdown">ABOUT<b class="caret"></b></a>
						              <ul class="dropdown-menu">
						                <li class="nav_li"><a href="who-we-are.html">WHO WE ARE</a></li>
						                <li class="nav_li"><a href="our-process.html">OUR PROCESS</a></li>
						              </ul>
						            </li>
						            <li><a href="services.html" class="nav-a">SERVICES</a></li>						
							        <li><a href="portfolio.html" class="nav-a">PORTFOLIO</a></li>
							        <li><a href="contact.html" class="nav-a">CONTACT</a></li>
							        <li><a href="quote.html"><button style="background: #EB1829;border:none;color: #fff;border-radius: 5px;">REQUEST A QUOTE</button></a></li>
							        <li><a href="refer.html"><button style="background: #EB1829;border:none;color: #fff;border-radius: 5px;">REFER & EARN</button></a></li>
								</ul>
						    </div>
						</div>
						<hr>
					</nav>
				</div>
			</div>
		</div>
	</div>

	<div class="refer-and-earn-header-bottom">
		<h1 class="refer-and-earn-header-bottom-h1">
			Refer and earn
		</h1>
	</div>

	<div class="container" style="margin-top: 40px; margin-bottom: 40px;">
		<div class="row">
			<div class="col-md-7">
				<h1 class="refer-and-earn-col-md-6-h1">
					Referring to Greymatter Agency is very easy:
				</h1>

				<table style="margin-bottom: 30px;">
					<tbody>
						<tr>
							<td style="width: 10%;" class="refer-and-earn-table-td">
								<img src="images/write.png" style="height: 40px;">
							</td>
							<td class="refer-and-earn-table-td">
								Fill out the form to sign up as an Greymatter Agency Client Referral Partner.
							</td>
						</tr>
					</tbody>
				</table>

				<table style="margin-bottom: 30px;">
					<tbody>
						<tr>
							<td style="width: 10%;" class="refer-and-earn-table-td">
								<img src="images/dashboard.png" style="height: 40px;">
							</td>
							<td class="refer-and-earn-table-td">
								Access your Referral Dashboard and share your unique referral link with your network.
							</td>
						</tr>
					</tbody>
				</table>

				<table>
					<tbody>
						<tr>
							<td style="width: 10%;" class="refer-and-earn-table-td">
								<img src="images/link.png" style="height: 40px;">
							</td>
							<td class="refer-and-earn-table-td">
								When your referral joins Greymatter Agency using your unique referral link, you get 5% commission on each project posted by your referral!
							</td>
						</tr>
					</tbody>
				</table>

				<img src="images/referral-image.png" style="margin-left: 70px; margin-top: 50px; margin-bottom: 50px;">

				<p class="refer-and-earn-faq-p">
					Greymatter Agency has grown mostly through word-of-mouth, and we're delighted to pay referral bonuses for projects you send our way! We'll also do our best to ensure that any project you refer has a terrific outcome.
					<br><br>
					If you refer a project to Greymatter Agency, we'll pay you 5% of the total project value straight to your bank account (upon completion & payment).
				</p>
			</div>

			<div class="col-md-5">
				<h1 class="refer-and-earn-col-md-6-h1">
					<em>
						Sign Up As An Greymatter Agency Referral Partner
					</em>
				</h1>

				<form class="form-horizontal" action="process_register.php" method="POST">
                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">First Name:</label>
                            <input type="text" class="form-control input" name="firstname" id="firstname" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Last Name:</label>
                            <input type="text" class="form-control input" name="lastname" id="lastname" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Email Address:</label>
                            <input type="email" class="form-control input" name="emailaddress" id="emailaddress" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Password:</label>
                            <input type="password" class="form-control input" name="password" id="password" required>
                       </div>
                    </div>

                     <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Occupation:</label>
                            <input type="text" class="form-control input" name="occupation" id="occupation" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Telephone:</label>
                            <input type="text" class="form-control input" name="phone" id="phone" required>
                       </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="sel1" class="refer-and-earn-col-md-6-form-label">Gender:</label>
                            <select class="form-control" name="gender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select> 
                        </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Address:</label>
                            <input type="text" class="form-control input" name="address" id="address" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">State:</label>
                            <input type="text" class="form-control input" name="state" id="state" required>
                       </div>
                    </div>

                    <div class="form-group form_wrap">      
                       <div class="col-sm-12">
                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Country:</label>
                            <select name="countrylist" id="countrylist" class="form-control" title="Select Your Country" style="width: 100%;">
	                            <option value="Afganistan">Afghanistan</option>
	                            <option value="Albania">Albania</option>
	                            <option value="Algeria">Algeria</option>
	                            <option value="American Samoa">American Samoa</option>
	                            <option value="Andorra">Andorra</option>
	                            <option value="Angola">Angola</option>
	                            <option value="Anguilla">Anguilla</option>
	                            <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
	                            <option value="Argentina">Argentina</option>
	                            <option value="Armenia">Armenia</option>
	                            <option value="Aruba">Aruba</option>
	                            <option value="Australia">Australia</option>
	                            <option value="Austria">Austria</option>
	                            <option value="Azerbaijan">Azerbaijan</option>
	                            <option value="Bahamas">Bahamas</option>
	                            <option value="Bahrain">Bahrain</option>
	                            <option value="Bangladesh">Bangladesh</option>
	                            <option value="Barbados">Barbados</option>
	                            <option value="Belarus">Belarus</option>
	                            <option value="Belgium">Belgium</option>
	                            <option value="Belize">Belize</option>
	                            <option value="Benin">Benin</option>
	                            <option value="Bermuda">Bermuda</option>
	                            <option value="Bhutan">Bhutan</option>
	                            <option value="Bolivia">Bolivia</option>
	                            <option value="Bonaire">Bonaire</option>
	                            <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
	                            <option value="Botswana">Botswana</option>
	                            <option value="Brazil">Brazil</option>
	                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
	                            <option value="Brunei">Brunei</option>
	                            <option value="Bulgaria">Bulgaria</option>
	                            <option value="Burkina Faso">Burkina Faso</option>
	                            <option value="Burundi">Burundi</option>
	                            <option value="Cambodia">Cambodia</option>
	                            <option value="Cameroon">Cameroon</option>
	                            <option value="Canada">Canada</option>
	                            <option value="Canary Islands">Canary Islands</option>
	                            <option value="Cape Verde">Cape Verde</option>
	                            <option value="Cayman Islands">Cayman Islands</option>
	                            <option value="Central African Republic">Central African Republic</option>
	                            <option value="Chad">Chad</option>
	                            <option value="Channel Islands">Channel Islands</option>
	                            <option value="Chile">Chile</option>
	                            <option value="China">China</option>
	                            <option value="Christmas Island">Christmas Island</option>
	                            <option value="Cocos Island">Cocos Island</option>
	                            <option value="Colombia">Colombia</option>
	                            <option value="Comoros">Comoros</option>
	                            <option value="Congo">Congo</option>
	                            <option value="Cook Islands">Cook Islands</option>
	                            <option value="Costa Rica">Costa Rica</option>
	                            <option value="Cote DIvoire">Cote D'Ivoire</option>
	                            <option value="Croatia">Croatia</option>
	                            <option value="Cuba">Cuba</option>
	                            <option value="Curaco">Curacao</option>
	                            <option value="Cyprus">Cyprus</option>
	                            <option value="Czech Republic">Czech Republic</option>
	                            <option value="Denmark">Denmark</option>
	                            <option value="Djibouti">Djibouti</option>
	                            <option value="Dominica">Dominica</option>
	                            <option value="Dominican Republic">Dominican Republic</option>
	                            <option value="East Timor">East Timor</option>
	                            <option value="Ecuador">Ecuador</option>
	                            <option value="Egypt">Egypt</option>
	                            <option value="El Salvador">El Salvador</option>
	                            <option value="Equatorial Guinea">Equatorial Guinea</option>
	                            <option value="Eritrea">Eritrea</option>
	                            <option value="Estonia">Estonia</option>
	                            <option value="Ethiopia">Ethiopia</option>
	                            <option value="Falkland Islands">Falkland Islands</option>
	                            <option value="Faroe Islands">Faroe Islands</option>
	                            <option value="Fiji">Fiji</option>
	                            <option value="Finland">Finland</option>
	                            <option value="France">France</option>
	                            <option value="French Guiana">French Guiana</option>
	                            <option value="French Polynesia">French Polynesia</option>
	                            <option value="French Southern Ter">French Southern Ter</option>
	                            <option value="Gabon">Gabon</option>
	                            <option value="Gambia">Gambia</option>
	                            <option value="Georgia">Georgia</option>
	                            <option value="Germany">Germany</option>
	                            <option value="Ghana">Ghana</option>
	                            <option value="Gibraltar">Gibraltar</option>
	                            <option value="Great Britain">Great Britain</option>
	                            <option value="Greece">Greece</option>
	                            <option value="Greenland">Greenland</option>
	                            <option value="Grenada">Grenada</option>
	                            <option value="Guadeloupe">Guadeloupe</option>
	                            <option value="Guam">Guam</option>
	                            <option value="Guatemala">Guatemala</option>
	                            <option value="Guinea">Guinea</option>
	                            <option value="Guyana">Guyana</option>
	                            <option value="Haiti">Haiti</option>
	                            <option value="Hawaii">Hawaii</option>
	                            <option value="Honduras">Honduras</option>
	                            <option value="Hong Kong">Hong Kong</option>
	                            <option value="Hungary">Hungary</option>
	                            <option value="Iceland">Iceland</option>
	                            <option value="India">India</option>
	                            <option value="Indonesia">Indonesia</option>
	                            <option value="Iran">Iran</option>
	                            <option value="Iraq">Iraq</option>
	                            <option value="Ireland">Ireland</option>
	                            <option value="Isle of Man">Isle of Man</option>
	                            <option value="Israel">Israel</option>
	                            <option value="Italy">Italy</option>
	                            <option value="Jamaica">Jamaica</option>
	                            <option value="Japan">Japan</option>
	                            <option value="Jordan">Jordan</option>
	                            <option value="Kazakhstan">Kazakhstan</option>
	                            <option value="Kenya">Kenya</option>
	                            <option value="Kiribati">Kiribati</option>
	                            <option value="Korea North">Korea North</option>
	                            <option value="Korea Sout">Korea South</option>
	                            <option value="Kuwait">Kuwait</option>
	                            <option value="Kyrgyzstan">Kyrgyzstan</option>
	                            <option value="Laos">Laos</option>
	                            <option value="Latvia">Latvia</option>
	                            <option value="Lebanon">Lebanon</option>
	                            <option value="Lesotho">Lesotho</option>
	                            <option value="Liberia">Liberia</option>
	                            <option value="Libya">Libya</option>
	                            <option value="Liechtenstein">Liechtenstein</option>
	                            <option value="Lithuania">Lithuania</option>
	                            <option value="Luxembourg">Luxembourg</option>
	                            <option value="Macau">Macau</option>
	                            <option value="Macedonia">Macedonia</option>
	                            <option value="Madagascar">Madagascar</option>
	                            <option value="Malaysia">Malaysia</option>
	                            <option value="Malawi">Malawi</option>
	                            <option value="Maldives">Maldives</option>
	                            <option value="Mali">Mali</option>
	                            <option value="Malta">Malta</option>
	                            <option value="Marshall Islands">Marshall Islands</option>
	                            <option value="Martinique">Martinique</option>
	                            <option value="Mauritania">Mauritania</option>
	                            <option value="Mauritius">Mauritius</option>
	                            <option value="Mayotte">Mayotte</option>
	                            <option value="Mexico">Mexico</option>
	                            <option value="Midway Islands">Midway Islands</option>
	                            <option value="Moldova">Moldova</option>
	                            <option value="Monaco">Monaco</option>
	                            <option value="Mongolia">Mongolia</option>
	                            <option value="Montserrat">Montserrat</option>
	                            <option value="Morocco">Morocco</option>
	                            <option value="Mozambique">Mozambique</option>
	                            <option value="Myanmar">Myanmar</option>
	                            <option value="Nambia">Nambia</option>
	                            <option value="Nauru">Nauru</option>
	                            <option value="Nepal">Nepal</option>
	                            <option value="Netherland Antilles">Netherland Antilles</option>
	                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
	                            <option value="Nevis">Nevis</option>
	                            <option value="New Caledonia">New Caledonia</option>
	                            <option value="New Zealand">New Zealand</option>
	                            <option value="Nicaragua">Nicaragua</option>
	                            <option value="Niger">Niger</option>
	                            <option value="Nigeria" selected="selected">Nigeria</option>
	                            <option value="Niue">Niue</option>
	                            <option value="Norfolk Island">Norfolk Island</option>
	                            <option value="Norway">Norway</option>
	                            <option value="Oman">Oman</option>
	                            <option value="Pakistan">Pakistan</option>
	                            <option value="Palau Island">Palau Island</option>
	                            <option value="Palestine">Palestine</option>
	                            <option value="Panama">Panama</option>
	                            <option value="Papua New Guinea">Papua New Guinea</option>
	                            <option value="Paraguay">Paraguay</option>
	                            <option value="Peru">Peru</option>
	                            <option value="Phillipines">Philippines</option>
	                            <option value="Pitcairn Island">Pitcairn Island</option>
	                            <option value="Poland">Poland</option>
	                            <option value="Portugal">Portugal</option>
	                            <option value="Puerto Rico">Puerto Rico</option>
	                            <option value="Qatar">Qatar</option>
	                            <option value="Republic of Montenegro">Republic of Montenegro</option>
	                            <option value="Republic of Serbia">Republic of Serbia</option>
	                            <option value="Reunion">Reunion</option>
	                            <option value="Romania">Romania</option>
	                            <option value="Russia">Russia</option>
	                            <option value="Rwanda">Rwanda</option>
	                            <option value="St Barthelemy">St Barthelemy</option>
	                            <option value="St Eustatius">St Eustatius</option>
	                            <option value="St Helena">St Helena</option>
	                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
	                            <option value="St Lucia">St Lucia</option>
	                            <option value="St Maarten">St Maarten</option>
	                            <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
	                            <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
	                            <option value="Saipan">Saipan</option>
	                            <option value="Samoa">Samoa</option>
	                            <option value="Samoa American">Samoa American</option>
	                            <option value="San Marino">San Marino</option>
	                            <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
	                            <option value="Saudi Arabia">Saudi Arabia</option>
	                            <option value="Senegal">Senegal</option>
	                            <option value="Seychelles">Seychelles</option>
	                            <option value="Sierra Leone">Sierra Leone</option>
	                            <option value="Singapore">Singapore</option>
	                            <option value="Slovakia">Slovakia</option>
	                            <option value="Slovenia">Slovenia</option>
	                            <option value="Solomon Islands">Solomon Islands</option>
	                            <option value="Somalia">Somalia</option>
	                            <option value="South Africa">South Africa</option>
	                            <option value="Spain">Spain</option>
	                            <option value="Sri Lanka">Sri Lanka</option>
	                            <option value="Sudan">Sudan</option>
	                            <option value="Suriname">Suriname</option>
	                            <option value="Swaziland">Swaziland</option>
	                            <option value="Sweden">Sweden</option>
	                            <option value="Switzerland">Switzerland</option>
	                            <option value="Syria">Syria</option>
	                            <option value="Tahiti">Tahiti</option>
	                            <option value="Taiwan">Taiwan</option>
	                            <option value="Tajikistan">Tajikistan</option>
	                            <option value="Tanzania">Tanzania</option>
	                            <option value="Thailand">Thailand</option>
	                            <option value="Togo">Togo</option>
	                            <option value="Tokelau">Tokelau</option>
	                            <option value="Tonga">Tonga</option>
	                            <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
	                            <option value="Tunisia">Tunisia</option>
	                            <option value="Turkey">Turkey</option>
	                            <option value="Turkmenistan">Turkmenistan</option>
	                            <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
	                            <option value="Tuvalu">Tuvalu</option>
	                            <option value="Uganda">Uganda</option>
	                            <option value="Ukraine">Ukraine</option>
	                            <option value="United Arab Erimates">United Arab Emirates</option>
	                            <option value="United Kingdom">United Kingdom</option>
	                            <option value="United States of America">United States of America</option>
	                            <option value="Uraguay">Uruguay</option>
	                            <option value="Uzbekistan">Uzbekistan</option>
	                            <option value="Vanuatu">Vanuatu</option>
	                            <option value="Vatican City State">Vatican City State</option>
	                            <option value="Venezuela">Venezuela</option>
	                            <option value="Vietnam">Vietnam</option>
	                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
	                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
	                            <option value="Wake Island">Wake Island</option>
	                            <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
	                            <option value="Yemen">Yemen</option>
	                            <option value="Zaire">Zaire</option>
	                            <option value="Zambia">Zambia</option>
	                            <option value="Zimbabwe">Zimbabwe</option>
							</select>
                       </div>
                    </div>

                    <div class="" style="text-align: center; padding-top: 20px;">
                    	<input type="submit" value="Sign Me Up As A Referral" class="refer-and-earn-col-md-6-form-button" id="signup_button">
                    </div>
                </form>
                <div id="messages" class="animated fadeIn"></div>
			</div>
		</div>
	</div>

	<div class="refer-and-earn-login">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="refer-and-earn-login-div">
						<h1 class="refer-and-earn-login-h1" style="margin-top: 0px;">
							<em>
								Login To Your Referral Account
							</em>
						</h1>

						<form name="" id="" class="form-horizontal" action="process_login.php" method="POST" style="width: 50%; margin: auto;">
		                    <div class="form-group form_wrap">      
		                       <div class="col-sm-12">
		                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Email:</label>
		                            <input type="email" class="form-control input" name="emailaddress" id="emailaddress" required>
		                       </div>
		                    </div>

		                    <div class="form-group form_wrap">      
		                       <div class="col-sm-12">
		                            <label for="sur" class="refer-and-earn-col-md-6-form-label">Password:</label>
		                            <input type="password" class="form-control input" name="password" id="password" required>
		                       </div>
		                    </div>

		                    <p>
		                    	<a href="" style="color: #fff;">
		                    		Recover Password!
		                    	</a>
		                    </p>

		                    <input type="submit" value="Login To Your Referral Account" class="refer-and-earn-col-md-6-login-form-button" id="signup_button">
		                </form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="refer-and-earn-faq">
		<div class="container">
			<h1 class="refer-and-earn-faq-h1">
				<em>
					Frequently Asked Questions
				</em>
			</h1>

			<div class="row">
				<div class="col-md-6">
					<h1 class="refer-and-earn-faq-h1">
						What’s the Referral Program?
					</h1>
					<p class="refer-and-earn-faq-p">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
				<div class="col-md-6">
					<h1 class="refer-and-earn-faq-h1">
						What's the Referral Dashboard?
					</h1>
					<p class="refer-and-earn-faq-p">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<h1 class="refer-and-earn-faq-h1">
						How do I refer?
					</h1>
					<p class="refer-and-earn-faq-p">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
				<div class="col-md-6">
					<h1 class="refer-and-earn-faq-h1">
						When will I get my reward?
					</h1>
					<p class="refer-and-earn-faq-p">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>