<?php  
  require_once('core/init.php');

  if (isset($_POST) & !empty($_POST)) {
    $firstname = filterUserInput($_POST['firstname']);
    $lastname = filterUserInput($_POST['lastname']);
    $emailaddress = filterUserInput($_POST['emailaddress']);
    $occupation = filterUserInput($_POST['occupation']);
    $phone = filterUserInput($_POST['phone']);
    $gender = filterUserInput($_POST['gender']);
    $address = filterUserInput($_POST['address']);
    $state = filterUserInput($_POST['state']);
    $country = filterUserInput($_POST['countrylist']);
    $password = $_POST['password'];
    $hashed = password_hash($password, PASSWORD_DEFAULT);
    $datetime = date("Y-m-d H:i:s");

    $stmt = $connection->prepare('SELECT email FROM register WHERE email = :emailaddress');
    $stmt->bindParam(emailaddress, $emailaddress, PDO::PARAM_INT);
    $stmt->execute();
    $email_count = $stmt->rowCount();

    if ($email_count >= 1) {
      echo "There is already a franchise with this email address, Kindly login.";
      die();
    }

    if (empty($firstname)) {
      echo "Please Enter Your First Name";
      die();
    }

    if (empty($lastname)) {
      echo "Please Enter Your Last Name";
      die();
    }

    if (empty($emailaddress)) {
      echo "Please Enter A Valid Email Address";
      die();
    }

    if (empty($occupation)) {
      echo "Please Enter Your Occupation";
      die();
    }

    if (empty($country)) {
      echo "Please Select Your Country";
      die();
    }

    if (empty($state)) {
      echo "Please Enter Your State";
      die();
    }

    if (empty($address)) {
      echo "Please Enter Your Address";
      die();
    }

    if (empty($phone)) {
      echo "Kindly Provide Your Phone Number";
      die();
    }

    if (!preg_match("/^[a-zA-Z ]*$/", $firstname)) {
      echo "Only letters and white space allowed";
      die();
    }

    if (!preg_match("/^[a-zA-Z ]*$/", $lastname)) {
      echo "Only letters and white space allowed";
      die();
    }

    if (!preg_match("/^[a-zA-Z ]*$/", $occupation)) {
      echo "Only letters and white space allowed";
      die();
    }

    if (!preg_match("/^[a-zA-Z ]*$/", $state)) {
      echo "Only letters and white space allowed";
      die();
    }

    if (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
      echo "Invalid Email Format, Please Enter A Valid Email Address.";
      die();
    } 

    $stmt = $connection->prepare('INSERT INTO register SET lastname = :lastname, firstname = :firstname, job = :occupation, email = :emailaddress, country = :country, state = :state, address = :address, password = :password, phone_number = :phone, gender = :gender, signup_date = :signup_date');
    $stmt->bindParam(':lastname', $lastname, PDO::PARAM_INT);
    $stmt->bindParam(':firstname', $firstname, PDO::PARAM_INT);
    $stmt->bindParam(':occupation', $occupation, PDO::PARAM_INT);
    $stmt->bindParam(':emailaddress', $emailaddress, PDO::PARAM_INT);
    $stmt->bindParam(':country', $country, PDO::PARAM_INT);
    $stmt->bindParam(':state', $state, PDO::PARAM_INT);
    $stmt->bindParam(':address', $address, PDO::PARAM_INT);
    $stmt->bindParam(':password', $hashed, PDO::PARAM_INT);
    $stmt->bindParam(':phone', $phone, PDO::PARAM_INT);
    $stmt->bindParam(':gender', $gender, PDO::PARAM_INT);
    $stmt->bindParam(':signup_date', $datetime, PDO::PARAM_INT);
    $stmt->execute();
    $id = $connection->lastInsertId();

    if ($id) {
      echo "Thank You, Registration Complete. Kindly Scroll To The Top Of The Page To Login";
    }else{
      echo "Franchise Registration Failed, Try again later.";
      die();
      // echo("Error description: " . mysqli_error($connection));
    }

    // $headers = "From: Efflux";
    // $membermail = "Dear, $firstname \n";
    // $membermail .="Message: Thank you for taking your precious time to register and we are looking forward to your hustle spirit. Always use your unique code below when posting a job. \n";
    // $membermail .= "Unique Code: $mycode";
    // mail($email, 'Franchise Registration', $membermail, $headers);

    // $headers = "From: Efflux";
    // $adminmail = "Dear, $firstname \n";
    // $adminmail .="Message: Thank you for taking your precious time to register. \n";
    // mail('info@greymatteragency.com', 'Franchise Registration', $adminmail, $headers);
  }
?>