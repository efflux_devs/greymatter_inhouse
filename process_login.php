<?php
  session_start();
  require_once('core/init.php');

  if (isset($_POST) & !empty($_POST)) {
    $email = $_POST['emailaddress'];
    $password = $_POST['password'];

    $sqlQuery = "SELECT * FROM register WHERE email= :email";
    $stmt = $connection->prepare($sqlQuery);
    $stmt->execute(array(':email' => $email));
    
    while ($row = $stmt->fetch()) {
      $id = $row['id'];
      $hashed = $row['password'];
      $email = $row['email'];

      if (password_verify($password, $hashed)) {
        session_start();
        $_SESSION['id'] = $id;
        $_SESSION['email'] = $email;
        header("location: profile.php");
      }else {
        echo "Invalid Username Or Password";
        die();
      }
    }
  }
?>